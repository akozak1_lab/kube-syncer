import threading
import time
import unittest
import unittest.mock
from unittest.mock import MagicMock, Mock

from kubernetes.client import ApiException
from mock_open import MockOpen

import syncer
import TestConstants


class syncUnitTests(unittest.TestCase):

    def setUp(self):
        self.mock_kubernetes_api = MagicMock()
        self.mock_kubernetes_api.list_namespace.return_value = TestConstants.NAMESPACE_LIST
        self.mock_kubernetes_api.list_namespaced_secret.return_value = TestConstants.SECRET_LIST
        self.mock_kubernetes_api.replace_namespaced_secret.return_value = True
        self.mock_kubernetes_api.list_namespaced_config_map.return_value = TestConstants.CONFIG_MAP_LIST
        self.mock_kubernetes_api.replace_namespaced_config_map.return_value = True

    def tearDown(self):
        self.mock_kubernetes_api.reset_mock()

    def test_init(self):
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)

        self.assertEqual(actual.last_namespace_resource_version, int(TestConstants.NAMESPACE_B_RESOURCE_VERSION))

    def test_stop(self):
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual.stop()

        self.assertTrue(actual.should_stop.isSet())

    def test_start(self):
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual.start()

        actual.restart_threads()
        actual.stop()
        actual.join_threads()

    def test_stop_on_restart(self):
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual.start()
        actual.stop()
        actual.restart_threads()

        actual.join_threads()

    def test_update_health_file_fail(self):
        mock_open = MockOpen()
        namespace_thread = MagicMock()
        namespace_thread.is_alive.return_value = False
        secret_thread = MagicMock()
        secret_thread.is_alive.return_value = False
        config_map_thread = MagicMock()
        config_map_thread.is_alive.return_value = True
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual.namespace_thread = namespace_thread
        actual.secret_thread = secret_thread
        actual.config_map_thread = config_map_thread

        with unittest.mock.patch("builtins.open", mock_open):
            actual.update_health_file()

        mock_open.assert_not_called()

    def test_update_health_file(self):
        mock_open = MockOpen()
        namespace_thread = MagicMock()
        namespace_thread.is_alive.return_value = True
        secret_thread = MagicMock()
        secret_thread.is_alive.return_value = True
        config_map_thread = MagicMock()
        config_map_thread.is_alive.return_value = True
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual.namespace_thread = namespace_thread
        actual.secret_thread = secret_thread
        actual.config_map_thread = config_map_thread

        with unittest.mock.patch("builtins.open", mock_open):
            actual.update_health_file()

        mock_open.assert_called_once()

    def test_get_namespace(self):
        sync = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual = sync.get_namespaces()

        self.assertEqual(actual, TestConstants.NAMESPACE_LIST)

    def test_copy_to_all_namespaces(self):
        sync = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        mock_function = Mock()

        sync.copy_to_all_namespaces(TestConstants.EVENT_D, mock_function)
        self.assertEqual(mock_function.call_count, 2)

    def test_copy_to_labeled_namespaces(self):
        sync = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        mock_function = Mock()

        sync.copy_to_labeled_namespaces(TestConstants.EVENT_E, mock_function)
        self.assertEqual(mock_function.call_count, 1)

    def test_set_secret_in_namespace(self):
        sync = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        # test no update needed
        sync.set_secret_in_namespace(TestConstants.NAMESPACE_A, TestConstants.NAME_A, TestConstants.SECRET_OBJECT_A)
        self.assertEqual(self.mock_kubernetes_api.replace_namespaced_secret.call_count, 0)
        self.assertEqual(self.mock_kubernetes_api.create_namespaced_secret.call_count, 0)

        self.tearDown()

        # Test is replaces namespace
        sync.set_secret_in_namespace(TestConstants.NAMESPACE_A, TestConstants.NAME_A, TestConstants.SECRET_OBJECT_C)
        self.assertEqual(self.mock_kubernetes_api.replace_namespaced_secret.call_count, 1)
        self.assertEqual(self.mock_kubernetes_api.create_namespaced_secret.call_count, 0)

        self.tearDown()
        # Test it created a new one
        sync.set_secret_in_namespace(TestConstants.NAMESPACE_A, TestConstants.NAME_C, TestConstants.SECRET_OBJECT_C)
        self.assertEqual(self.mock_kubernetes_api.replace_namespaced_secret.call_count, 0)
        self.assertEqual(self.mock_kubernetes_api.create_namespaced_secret.call_count, 1)

    def test_set_config_map_in_namespace(self):
        sync = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        # test no update needed
        sync.set_config_map_in_namespace(TestConstants.NAMESPACE_A, TestConstants.NAME_A, TestConstants.CONFIG_MAP_OBJECT_A)
        self.assertEqual(self.mock_kubernetes_api.replace_namespaced_config_map.call_count, 0)
        self.assertEqual(self.mock_kubernetes_api.create_namespaced_config_map.call_count, 0)

        self.tearDown()

        # Test is replaces namespace
        sync.set_config_map_in_namespace(TestConstants.NAMESPACE_A, TestConstants.NAME_A, TestConstants.CONFIG_MAP_OBJECT_C)
        self.assertEqual(self.mock_kubernetes_api.replace_namespaced_config_map.call_count, 1)
        self.assertEqual(self.mock_kubernetes_api.create_namespaced_config_map.call_count, 0)

        self.tearDown()
        # Test it created a new one
        sync.set_config_map_in_namespace(TestConstants.NAMESPACE_A, TestConstants.NAME_C, TestConstants.CONFIG_MAP_OBJECT_C)
        self.assertEqual(self.mock_kubernetes_api.replace_namespaced_config_map.call_count, 0)
        self.assertEqual(self.mock_kubernetes_api.create_namespaced_config_map.call_count, 1)

    @unittest.mock.patch("syncer.watch.Watch")
    def test_monitor_secrets(self, mock_watch):
        mock_watch.return_value.stream.return_value = TestConstants.SECRET_EVENT_LIST
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual_thread_a = threading.Thread(target=actual.monitor_secrets, name="Secrets Monitor Test A")
        actual_thread_a.start()
        while not mock_watch.return_value.stream.called:
            time.sleep(5)
            pass
        actual.should_stop.set()
        actual_thread_a.join()

        self.tearDown()

        mock_watch.return_value.stream.side_effect = ApiException(status=410, reason="Expired: too old resource version: 41148409 (53476547)")
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual_thread_b = threading.Thread(target=actual.monitor_secrets, name="Secrets Monitor Test B")
        actual_thread_b.start()
        while not mock_watch.return_value.stream.called:
            pass
        actual.should_stop.set()
        actual_thread_a.join()
        self.tearDown()

    @unittest.mock.patch("syncer.watch.Watch")
    def test_monitor_config_map(self, mock_watch):
        mock_watch.return_value.stream.return_value = TestConstants.CONFIG_MAP_EVENT_LIST
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual_thread_a = threading.Thread(target=actual.monitor_config_maps, name="ConfigMap Monitor Test A")
        actual_thread_a.start()
        while not mock_watch.return_value.stream.called:
            time.sleep(5)
            pass
        actual.should_stop.set()
        actual_thread_a.join()

        self.tearDown()

        mock_watch.return_value.stream.side_effect = ApiException(status=410, reason="Expired: too old resource version: 41148409 (53476547)")
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual_thread_b = threading.Thread(target=actual.monitor_config_maps, name="ConfigMap Monitor Test B")
        actual_thread_b.start()
        while not mock_watch.return_value.stream.called:
            pass
        actual.should_stop.set()
        actual_thread_a.join()
        self.tearDown()

    @unittest.mock.patch("syncer.watch.Watch")
    @unittest.mock.patch("syncer.Syncer.restart_threads")
    def test_monitor_namespaces(self, mock_restart_threads, mock_watch):
        mock_restart_threads.return_value = True
        mock_watch.return_value.stream.return_value = TestConstants.NAMESPACE_EVENT_LIST
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)
        actual_thread_a = threading.Thread(target=actual.monitor_namespaces, name="Namespace Monitor Test A")
        actual_thread_a.start()
        while not mock_watch.return_value.stream.called:
            pass
        actual.should_stop.set()
        actual_thread_a.join()

        self.tearDown()

        mock_watch.return_value.stream.side_effect = ApiException(status=410, reason="Expired: too old resource version: 41148409 (53476547)")
        actual = syncer.Syncer(MagicMock(), self.mock_kubernetes_api, TestConstants.TIMEOUT_INTERVAL)

        actual_thread_b = threading.Thread(target=actual.monitor_namespaces, name="Namespace Monitor Test B")
        actual_thread_b.start()
        while not mock_watch.return_value.stream.called:
            pass
        actual.should_stop.set()
        actual_thread_a.join()
        self.tearDown()


def function(a, b, c):
    pass


if __name__ == "__main__":
    unittest.main()
