from copy import deepcopy
from unittest.mock import MagicMock

ANNOTATIONS_A = {"kube-syncer/auto": "", "temp_a": "today"}
ANNOTATIONS_A_RESULT = {"temp_a": "today"}
ANNOTATIONS_B = {"kube-syncer/label": "test=app", "temp_b": "today"}
ANNOTATIONS_B_RESULT = {"temp_b": "today"}
ANNOTATIONS_C = {}
ANNOTATIONS_D = {"kube-syncer/last_updated": "Today"}
ANNOTATIONS_E = {"kube-syncer/last_updated": "Today", "test": "true", "Zelda": "loud", "Kenshin": "cool", "Matt": "sucks",
                 "kube-syncer/purge-annotations": "test,Zelda, Matt", "kube-syncer/purge-labels": "app.kubernetes.io/instance"}
EXPECTED_PURGE_LIST = {"kube-syncer/last_updated": "Today", "Kenshin": "cool", "kube-syncer/purge-annotations": "test,Zelda, Matt",
                       "kube-syncer/purge-labels": "app.kubernetes.io/instance"}
COPIED_OBJECT = MagicMock()
COPIED_OBJECT.metadata.annotations = ANNOTATIONS_D
NOT_COPIED_OBJECT = MagicMock()

LABEL_A = {"Friday": "Today"}
LABEL_B = {"last_updated": "Today", "test": "true", "Zelda": "loud", "app.kubernetes.io/instance": "test_deployment", "Kenshin": "cool", "Matt": "sucks"}
EXPECTED_PURGE_LABEL_LIST = {"last_updated": "Today", "test": "true", "Zelda": "loud", "Kenshin": "cool", "Matt": "sucks"}

EVENT_COPIED = {}
EVENT_COPIED['object'] = COPIED_OBJECT

EVENT_NOT_COPIED = {}
EVENT_NOT_COPIED['object'] = NOT_COPIED_OBJECT


def get_annotation_a():
    return deepcopy(ANNOTATIONS_A)


def get_annotation_b():
    return deepcopy(ANNOTATIONS_B)


def get_annotation_c():
    return deepcopy(ANNOTATIONS_C)


NAMESPACE1 = "test1"
RESOURCE_VERSION1 = "1"
UID1 = "931b2841-fb2f-479d-b5c0-8679af49b211"
CREATION_TIMESTAMP1 = "2021-11-13T14:23:33.602074"
TIMEOUT_INTERVAL = 10

LABEL = "test=app"

# In TestConstants.py
NAMESPACE_A_NAME = "BLAH"
NAMESPACE_A_RESOURCE_VERSION = "1234"
NAMESPACE_A_LABELS = {"test": "app"}
NAMESPACE_A_PHASE = "Active"
NAMESPACE_B_NAME = "OTHER"
NAMESPACE_B_RESOURCE_VERSION = "2345"
NAMESPACE_B_LABELS = {"today": "matt"}
NAMESPACE_B_PHASE = "Active"
NAMESPACE_C_NAME = "MATT"
NAMESPACE_C_RESOURCE_VERSION = "345"
NAMESPACE_C_LABELS = {}
NAMESPACE_C_PHASE = "Active"
NAMESPACE_D_NAME = "Andrew"
NAMESPACE_D_RESOURCE_VERSION = "3645"
NAMESPACE_D_LABELS = {}
NAMESPACE_D_PHASE = "Terminating"
LATEST_RESOURCE_VERSION = 2311


def get_namespace_a():
    return get_namespace(NAMESPACE_A_NAME, NAMESPACE_A_RESOURCE_VERSION, deepcopy(NAMESPACE_A_LABELS), NAMESPACE_A_PHASE)


def get_namespace_b():
    return get_namespace(NAMESPACE_B_NAME, NAMESPACE_B_RESOURCE_VERSION, deepcopy(NAMESPACE_B_LABELS), NAMESPACE_B_PHASE)


def get_namespace_c():
    return get_namespace(NAMESPACE_C_NAME, NAMESPACE_C_RESOURCE_VERSION, deepcopy(NAMESPACE_C_LABELS), NAMESPACE_C_PHASE)


def get_namespace_d():
    return get_namespace(NAMESPACE_D_NAME, NAMESPACE_D_RESOURCE_VERSION, deepcopy(NAMESPACE_D_LABELS), NAMESPACE_D_PHASE)


def get_namespace(name, resource_version, labels, phase):
    new_namespace = MagicMock()
    new_namespace.metadata.name = name
    new_namespace.metadata.resource_version = resource_version
    new_namespace.metadata.labels = labels
    new_namespace.metadata.namespace = "test"
    new_namespace.kind = "Namespace"
    new_namespace.status.phase = phase
    return new_namespace


NAMESPACE_A = get_namespace_a()
NAMESPACE_B = get_namespace_b()
NAMESPACE_C = get_namespace_c()
NAMESPACE_D = get_namespace_d()

NAMESPACE_LIST = MagicMock()
NAMESPACE_LIST.items = [
    NAMESPACE_A,
    NAMESPACE_B,
    NAMESPACE_C
]

LONG_NAMESPACE_LIST = MagicMock()
LONG_NAMESPACE_LIST.items = [
    NAMESPACE_D,
    NAMESPACE_B,
    NAMESPACE_C,
    NAMESPACE_A,
    NAMESPACE_B,
    NAMESPACE_C,
    NAMESPACE_A,
    NAMESPACE_B,
    NAMESPACE_C
]

OBJECT_LIST = [
    NAMESPACE_A,
    NAMESPACE_B,
    NAMESPACE_C
]

NAME_A = "secret_a_name"
NAME_B = "secret_b_name"
NAME_C = "unknown"
SECRET_OBJECT_A = MagicMock()
SECRET_OBJECT_A.metadata.namespace = NAMESPACE_A_NAME
SECRET_OBJECT_A.metadata.name = NAME_A
SECRET_OBJECT_A.metadata.resource_version = NAMESPACE_A_RESOURCE_VERSION
SECRET_OBJECT_A.metadata.annotations = deepcopy(ANNOTATIONS_A)

SECRET_OBJECT_B = MagicMock()
SECRET_OBJECT_B.metadata.namespace = NAMESPACE_A_NAME
SECRET_OBJECT_B.metadata.name = NAME_B
SECRET_OBJECT_B.metadata.resource_version = NAMESPACE_B_RESOURCE_VERSION
SECRET_OBJECT_B.metadata.annotations = ANNOTATIONS_B

SECRET_OBJECT_C = MagicMock()
SECRET_OBJECT_C.metadata.namespace = NAMESPACE_A_NAME
SECRET_OBJECT_C.metadata.name = NAME_A
SECRET_OBJECT_C.metadata.resource_version = '5678'
SECRET_OBJECT_C.metadata.annotations = deepcopy(ANNOTATIONS_A)

SECRET_OBJECT_D = MagicMock()
SECRET_OBJECT_D.metadata.namespace = NAMESPACE_A_NAME
SECRET_OBJECT_D.metadata.name = NAME_B
SECRET_OBJECT_D.metadata.resource_version = NAMESPACE_B_RESOURCE_VERSION
SECRET_OBJECT_D.metadata.annotations = ANNOTATIONS_D

SECRET_LIST = MagicMock()
SECRET_LIST.items = [
    SECRET_OBJECT_A,
    SECRET_OBJECT_B
]

CONFIG_MAP_OBJECT_A = MagicMock()
CONFIG_MAP_OBJECT_A.metadata.namespace = NAMESPACE_A_NAME
CONFIG_MAP_OBJECT_A.metadata.name = NAME_A
CONFIG_MAP_OBJECT_A.metadata.resource_version = NAMESPACE_A_RESOURCE_VERSION
CONFIG_MAP_OBJECT_A.metadata.annotations = deepcopy(ANNOTATIONS_A)

CONFIG_MAP_OBJECT_B = MagicMock()
CONFIG_MAP_OBJECT_B.metadata.namespace = NAMESPACE_A_NAME
CONFIG_MAP_OBJECT_B.metadata.name = NAME_B
CONFIG_MAP_OBJECT_B.metadata.resource_version = NAMESPACE_B_RESOURCE_VERSION
CONFIG_MAP_OBJECT_B.metadata.annotations = ANNOTATIONS_B

CONFIG_MAP_OBJECT_C = MagicMock()
CONFIG_MAP_OBJECT_C.metadata.namespace = NAMESPACE_A_NAME
CONFIG_MAP_OBJECT_C.metadata.name = NAME_A
CONFIG_MAP_OBJECT_C.metadata.resource_version = '5678'
CONFIG_MAP_OBJECT_C.metadata.annotations = deepcopy(ANNOTATIONS_A)

CONFIG_MAP_LIST = MagicMock()
CONFIG_MAP_LIST.items = [
    CONFIG_MAP_OBJECT_A,
    CONFIG_MAP_OBJECT_B
]


EVENT_A = {}
EVENT_A['object'] = get_namespace_a()
EVENT_A['type'] = 'TERMINATED'
EVENT_B = {}
EVENT_B['object'] = get_namespace_b()
EVENT_B['type'] = 'MODIFIED'
EVENT_C = {}
EVENT_C['object'] = get_namespace_d()
EVENT_C['type'] = 'MODIFIED'
EVENT_D = {}
EVENT_D['object'] = SECRET_OBJECT_A
EVENT_D['type'] = 'ADDED'
EVENT_E = {}
EVENT_E['object'] = SECRET_OBJECT_B
EVENT_E['type'] = 'ADDED'
EVENT_F = {}
EVENT_F['object'] = get_namespace_d()
EVENT_F['type'] = 'MODIFIED'

SECRET_EVENT_A = {}
SECRET_EVENT_A['type'] = 'ADDED'
SECRET_EVENT_A['object'] = SECRET_OBJECT_A
SECRET_EVENT_B = {}
SECRET_EVENT_B['type'] = 'MODIFIED'
SECRET_EVENT_B['object'] = SECRET_OBJECT_B
SECRET_EVENT_C = {}
SECRET_EVENT_C['type'] = 'ADDED'
SECRET_EVENT_C['object'] = SECRET_OBJECT_C
SECRET_EVENT_D = {}
SECRET_EVENT_D['type'] = 'DELETED'
SECRET_EVENT_D['object'] = SECRET_OBJECT_D

SECRET_EVENT_LIST = [
    SECRET_EVENT_A,
    SECRET_EVENT_B,
    SECRET_EVENT_D,
    SECRET_EVENT_B,
    SECRET_EVENT_A,
    SECRET_EVENT_B,
    SECRET_EVENT_A,
    SECRET_EVENT_B
]


CONFIG_MAP_EVENT_LIST = [
    SECRET_EVENT_A,
    SECRET_EVENT_B,
    SECRET_EVENT_D,
    SECRET_EVENT_B,
    SECRET_EVENT_A,
    SECRET_EVENT_B,
    SECRET_EVENT_A,
    SECRET_EVENT_B
]

NAMESPACE_EVENT_LIST = [
    SECRET_EVENT_C,
    SECRET_EVENT_B,
    SECRET_EVENT_A,
    SECRET_EVENT_B,
    SECRET_EVENT_A,
    SECRET_EVENT_B,
    SECRET_EVENT_A,
    SECRET_EVENT_B
]

API_EXCEPTION_A = MagicMock()
API_EXCEPTION_A.reason = "Expired: too old resource version: 41148409 (53476547)"
API_EXCEPTION_B = MagicMock()
API_EXCEPTION_B.reason = "Expired: too old resource version: 41148409 (ABCD)"
API_EXCEPTION_C = MagicMock()
API_EXCEPTION_C.reason = "Expired: too old resource version: ABCD (53476547)"
API_EXCEPTION_D = MagicMock()
API_EXCEPTION_D.reason = "Expired: too old resource version: 41148409 (53476547)"
