"""Utility class for syncer tool.

Static Methods to make moving secrets and configmaps easier.
"""

import hashlib
import os
import re
from datetime import datetime


class SyncerUtils(object):
    """Utilities for syncer class.
    """

    @staticmethod
    def find_namespace(event, namespace_list):
        """Return the namespace object from an event.

        Args:
            event (string): event used to find if the namespace exists
            namespace_list (V1NamespaceList): List of namespaces

        Returns:
            V1Namespace: Namespace that matches the event object
        """
        if event.kind == "Namespace":
            name = event.metadata.name
        else:
            name = event.metadata.namespace

        for namespace in namespace_list.items:
            if namespace.metadata.name == name:
                return namespace

    @staticmethod
    def get_most_recent_resource_version_from(objects):
        """From list of object find latest resource version.

        Args:
            objects (List[obj]): List of objects with resource versions.

        Returns:
            int: latest resource version
        """
        last_seen_resource_version = None
        for object in objects:
            if last_seen_resource_version is None or last_seen_resource_version < int(object.metadata.resource_version):
                last_seen_resource_version = int(object.metadata.resource_version)

        return last_seen_resource_version

    @staticmethod
    def is_copied_object(event):
        """Determine if an event is a copied object.

        Args:
            event (CoreV1Event): Event to analyze

        Returns:
            boolean: Returns if an event is copied.
        """
        source_annotations = ["kube-syncer/last_updated", "kube-syncer/source_namespace", "kube-syncer/source_resource_version"]
        if event['object'].metadata.annotations:
            for annotation in event['object'].metadata.annotations:
                if annotation in source_annotations:
                    return True
        return False

    @staticmethod
    def clean_annotations(annotations):
        """Clean up annotations by removing reserve annotations.

        Args:
            annotations (Dict): Annotations to clean.

        Returns:
            Dictionary: Cleaned annotations
        """
        reserved_annotations = ['kube-syncer/auto', 'kube-syncer/label', 'kube-syncer/last_updated', 'kube-syncer/source_namespace',
                                'kube-syncer/source_resource_version', 'kube-syncer/purge-annotations', 'kube-syncer/purge-labels']
        new_annotations = {}
        for annotation in annotations:
            if annotation not in reserved_annotations:
                new_annotations[annotation] = annotations[annotation]

        return new_annotations

    @staticmethod
    def clean_metadata(metadata):
        """Remove kubernetes defined metadata.

        Args:
            metadata (V1ObjectMeta): Metadata object to clean

        Returns:
            V1ObjectMeta: Clean metadata
        """
        metadata.uid = None
        metadata.creation_timestamp = None
        metadata.resource_version = None
        return metadata

    @staticmethod
    def add_source_information(annotations, source_namespace, source_resource_version):
        """Add source information to the copied object.

        Args:
            annotations: annotations list
            source_namespace: name of the source namespace
            source_resource_version: Revision number of the source object

        Returns:
            Dictionary: dictionary of the annotations
        """
        annotations['kube-syncer/last_updated'] = datetime.utcnow().isoformat()
        annotations['kube-syncer/source_namespace'] = source_namespace
        annotations['kube-syncer/source_resource_version'] = source_resource_version
        return annotations

    @staticmethod
    def purge_dictionary(annotations, object_dict, purge_key):
        """Remove list of keys from dictionary from purge_key.

        Args:
            annotations: Annotations to locate purge_key
            object_dict: Dictionary to be purged
            purge_key: Annotation key with list of keys to purge

        Returns:
            Dictionary: Dictionary with purged keys.
        """
        purged_object = {}
        if purge_key not in annotations:
            return object_dict

        purge_value = annotations[purge_key]

        purge_list = [x.strip() for x in purge_value.split(',')]

        for object in object_dict:
            if object not in purge_list:
                purged_object[object] = object_dict[object]
        return purged_object

    @staticmethod
    def update_metadata(new_namespace, obj):
        """Update the objects metadata by performing a few steps.

        Purges annotations and labels, removes restricted annotations, adds
        the source information and removes controlled metadata.

        Args:
            new_namespace (str): namespace name to update the object
            obj: Object to update metadata

        Returns:
            obj: Object with updated metadata
        """
        obj.metadata.annotations = SyncerUtils.purge_dictionary(obj.metadata.annotations, obj.metadata.annotations, 'kube-syncer/purge-annotations')
        obj.metadata.labels = SyncerUtils.purge_dictionary(obj.metadata.annotations, obj.metadata.labels, 'kube-syncer/purge-labels')
        obj.metadata.annotations = SyncerUtils.clean_annotations(obj.metadata.annotations)
        obj.metadata.annotations = SyncerUtils.add_source_information(obj.metadata.annotations, obj.metadata.namespace, obj.metadata.resource_version)
        obj.metadata = SyncerUtils.clean_metadata(obj.metadata)
        obj.metadata.namespace = new_namespace
        return obj

    @staticmethod
    def is_label_in_namespace(namespace, label):
        """Check if label exists in the provided namespace.

        Args:
            namespace (V1Namespace): Namespace object to check
            label (str): Label to locate in namespace

        Returns:
            boolean: If the provided label exists in namespace.
        """
        if namespace.metadata.labels:
            return tuple(label.split('=', maxsplit=1)) in namespace.metadata.labels.items()
        return False

    @staticmethod
    def is_most_recent_resource_version(event, last_seen_version):
        """Return if an event is the latest resource version.

        Args:
            event (CoreV1Event): The event to analyze resource version
            last_seen_version (int): latest resource version

        Returns:
            bool: If event is the latest resource version
        """
        if last_seen_version is None or last_seen_version < int(event['object'].metadata.resource_version):
            return True

        return False

    @staticmethod
    def is_namespace_event_relevant(logger, event, namespace_list):
        """Determine if a namespace is relevant to syncer.

        Args:
            logger (Logger): logger provided for the method
            event (CoreV1Event): an event to analyze for relevance
            namespace_list (V1NamespaceList): A list of namespaces from the
                cluster

        Returns:
            [boolean]: Returns if a namespace matters for processing
        """
        if event['type'] == 'MODIFIED':
            existing_namespace = SyncerUtils.find_namespace(event['object'], namespace_list)
            if event['object'].status.phase == "Terminating" and existing_namespace:
                return True
            if not existing_namespace:
                logger.debug("Namespace Modified doesn't exist")
                return False
            if event['object'].metadata.labels == existing_namespace.metadata.labels:
                logger.debug("Namespace Modified doesn't change labels")
                return False
        return True

    @staticmethod
    def extract_resource_version_from_exception(exception):
        """Regex's out the latest resource version and returns.

        Args:
            exception (str): An exception message

        Returns:
            int: the latest resource version number
        """
        regex = r"Expired: too old resource version: \d+ \((\d+)\)"
        result = re.search(regex, exception.reason)
        if not result:
            return None

        match = result.group(1)

        return int(match)

    @staticmethod
    def is_equal(object1, object2):
        """Return a boolean whether two objects are equal.

        Args:
            object1: First object to compare
            object2: Second object to compare.

        Returns:
            [boolean]: If the two objects are equal.
        """
        return SyncerUtils.calculate_hash(object1) == SyncerUtils.calculate_hash(object2)

    @staticmethod
    def calculate_hash(object):
        """Return the hash of an object.

        Args:
            object: Object to calculate the hash against.

        Returns:
            [str]: Hash value of the object
        """
        hash = hashlib.sha256()

        if hasattr(object, 'data'):
            hash.update(str(object.data).encode())

        if hasattr(object, 'binaryData'):
            hash.update(str(object.binaryData).encode())

        if hasattr(object, 'stringData'):
            hash.update(str(object.stringData).encode())

        if hasattr(object, 'annotations'):
            hash.update(str(SyncerUtils.clean_annotations(object.annotations)).encode())

        if hasattr(object, 'labels'):
            hash.update(str(object.labels).encode())

        return hash.hexdigest()

    @staticmethod
    def update_healthy(logger):
        """Update the health file to show program is running all threads.
        """
        with open(f"{os.getcwd()}/tmp/health", "w") as health_file:
            health_file.flush()

    @staticmethod
    def filter_active_only_namespaces(logger, namespace_list):
        """Filter out items in namespace list that are not Active.

        Args:
            logger (Logger): logger provided for the method
            namespace_list (V1NamespaceList): A list of namespaces from the
                cluster
        """
        new_namespace_list = []
        for namespace in namespace_list.items:
            if namespace.status.phase == "Active":
                new_namespace_list.append(namespace)

        namespace_list.items = new_namespace_list

        return namespace_list
