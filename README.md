# akozak1-lab/kube-syncer
## Description
A tool to copy secrets and configmaps across namespaces in Kubernetes.

## Deployment
A sample kubernetes deployment is located under the deployment folder.
```bash
kubectl apply -f deployments/kubernetes/manifests
```

### kustomize
```
kubectl create namespace kube-syncer
kubectl apply -k deployments/kubernetes
```

### Enviroment Variables
|Name|Description|
|----|-----------|
|TIMEOUT_INTERVAL|Used to exit the watch loops for the threads. (Should be less than the pod GracefulterminationTime).|
| LOGLEVEL|A log level between DEBUG and ERROR (more information located here [python logging](https://docs.python.org/3/howto/logging.html#when-to-use-logging))|

## Usage
To copy secrets or configmaps across namespaces you have to set an annotation on the object you would like to copy. You have a choice of adding "kube-syncer/auto: ''" or "kube-syncer/label: 'environment=testing'". The auto option will copy the object to every namespace in the cluster. If you set the label option it will only copy to namespaces with the value you set on the annotation.

*Reminder about kubernetes namespaces, the kube-syncer/label utilizes the format presented by kubectl get ns --show-labels. Also, annotations and labels in kubernetes keys must be unique.*

| Annotation | Description |
|------------|-------------|
| kube-syncer/auto: "" | Set this key in the annotation to sync to all namespaces |
| kube-syncer/label: "\<label key\>=\<label value\>" | Set this key to in the annotation to sync to all namespaces with the \<label key\>=\<label value\> |
| kube-syncer/purge-annotations: "\<annotation key1\>,\<annotation key2\>,\<annotation keyn\>" | Set this with a comma seperated list of annotations you would like to be removed from the copied object |
| kube-syncer/purge-labels: "\<label key1\>,\<label key2\>,\<label keyn\>" | Set this with a comma seperated list of labels you would like to be removed from the copied object |

All labels and annotations will be copied to the new namespace unless the purge annotations are used, with a few limitations. It will remove the original annotation set and replace it with three annotations.
```source
kube-syncer/last_updated
kube-syncer/source_namespace
kube-syncer/source_resource_version
```
