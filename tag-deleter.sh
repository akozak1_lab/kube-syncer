#!/usr/bin/env bash

echo "Looking for tag $1"
#  | jq -sRr @uri
DIGEST=$(curl -I "http://registry.plastroltech.local/v2/kube-syncer/manifests/$1" -H "Accept: application/vnd.docker.distribution.manifest.v2+json" | grep Digest | awk '{ print $2 }')
export DIGEST

if [ -z "$DIGEST" ]; then
  echo "Could not find digest for given tag $1"
  exit 1
fi

echo "Deleting tag $1 with digest $DIGEST"
curl -X DELETE -vv "http://registry.plastroltech.local/v2/kube-syncer/manifests/${DIGEST%?}"
