import os
import unittest
import unittest.mock
from datetime import datetime
from unittest.mock import MagicMock

from kubernetes.client import V1ObjectMeta
from mock_open import MockOpen

import syncerUtils
import TestConstants


class syncUnitTests(unittest.TestCase):

    def test_find_namespace(self):
        # In my unit tests
        actual = syncerUtils.SyncerUtils.find_namespace(TestConstants.NAMESPACE_A, TestConstants.NAMESPACE_LIST)
        self.assertEqual(actual.metadata.name, TestConstants.NAMESPACE_A_NAME)

    def test_get_most_recent_resource_version_from(self):

        actual = syncerUtils.SyncerUtils.get_most_recent_resource_version_from(TestConstants.OBJECT_LIST)
        self.assertEqual(actual, int(TestConstants.NAMESPACE_B_RESOURCE_VERSION))

    @unittest.mock.patch("syncerUtils.datetime")
    def test_add_source_information(self, mock_datetime):
        current_time = datetime.utcnow().isoformat()

        mock_datetime.utcnow.return_value.isoformat.return_value = current_time

        test_add_source_information_object = syncerUtils.SyncerUtils.add_source_information(TestConstants.get_annotation_c(), TestConstants.NAMESPACE1,
                                                                                            TestConstants.RESOURCE_VERSION1)

        expected_result = {}
        expected_result['kube-syncer/last_updated'] = current_time
        expected_result['kube-syncer/source_namespace'] = TestConstants.NAMESPACE1
        expected_result['kube-syncer/source_resource_version'] = TestConstants.RESOURCE_VERSION1

        self.assertEqual(expected_result, test_add_source_information_object)

    def test_clean_metadata(self):
        test_clean_metadata_object = syncerUtils.SyncerUtils.clean_metadata(V1ObjectMeta(
            uid=TestConstants.UID1, creation_timestamp=TestConstants.CREATION_TIMESTAMP1, resource_version=TestConstants.RESOURCE_VERSION1))

        self.assertEqual(V1ObjectMeta(), test_clean_metadata_object)

    def test_clean_annotations(self):
        actual_annotations_a = syncerUtils.SyncerUtils.clean_annotations(TestConstants.get_annotation_a())
        self.assertEqual(actual_annotations_a, TestConstants.ANNOTATIONS_A_RESULT)

        actual_annotations_b = syncerUtils.SyncerUtils.clean_annotations(TestConstants.get_annotation_b())
        self.assertEqual(actual_annotations_b, TestConstants.ANNOTATIONS_B_RESULT)

    def test_update_metadata(self):
        actual = syncerUtils.SyncerUtils.update_metadata(TestConstants.NAMESPACE_B_NAME, TestConstants.SECRET_OBJECT_A)

        self.assertEqual(actual.metadata.namespace, TestConstants.NAMESPACE_B_NAME)

    def test_is_label_in_namespace(self):
        actual_a = syncerUtils.SyncerUtils.is_label_in_namespace(TestConstants.NAMESPACE_A, TestConstants.LABEL)
        self.assertTrue(actual_a)

        actual_b = syncerUtils.SyncerUtils.is_label_in_namespace(TestConstants.NAMESPACE_B, TestConstants.LABEL)
        self.assertFalse(actual_b)

        actual_c = syncerUtils.SyncerUtils.is_label_in_namespace(TestConstants.NAMESPACE_D, TestConstants.LABEL)
        self.assertFalse(actual_c)

    def test_is_most_recent_resource_version(self):
        actual_a = syncerUtils.SyncerUtils.is_most_recent_resource_version(TestConstants.EVENT_A, TestConstants.LATEST_RESOURCE_VERSION)
        self.assertFalse(actual_a)

        actual_b = syncerUtils.SyncerUtils.is_most_recent_resource_version(TestConstants.EVENT_B, TestConstants.LATEST_RESOURCE_VERSION)
        self.assertTrue(actual_b)

    def test_is_namespace_event_relevant(self):
        actual_a = syncerUtils.SyncerUtils.is_namespace_event_relevant(MagicMock(), TestConstants.EVENT_A, TestConstants.NAMESPACE_LIST)
        self.assertTrue(actual_a)

        actual_b = syncerUtils.SyncerUtils.is_namespace_event_relevant(MagicMock(), TestConstants.EVENT_B, TestConstants.NAMESPACE_LIST)
        self.assertFalse(actual_b)

        actual_c = syncerUtils.SyncerUtils.is_namespace_event_relevant(MagicMock(), TestConstants.EVENT_C, TestConstants.NAMESPACE_LIST)
        self.assertFalse(actual_c)

    @unittest.mock.patch("syncerUtils.SyncerUtils.find_namespace")
    def test_is_namespace_event_relevant_terminating(self, mock_find_namespace):
        mock_find_namespace.return_value = TestConstants.NAMESPACE_D
        actual_d = syncerUtils.SyncerUtils.is_namespace_event_relevant(MagicMock(), TestConstants.EVENT_F, TestConstants.NAMESPACE_LIST)

        self.assertTrue(actual_d)

    def test_extract_resource_version_from_exception(self):
        actual_a = syncerUtils.SyncerUtils.extract_resource_version_from_exception(TestConstants.API_EXCEPTION_A)
        self.assertEqual(actual_a, 53476547)

        actual_b = syncerUtils.SyncerUtils.extract_resource_version_from_exception(TestConstants.API_EXCEPTION_B)
        self.assertEqual(actual_b, None)

        actual_c = syncerUtils.SyncerUtils.extract_resource_version_from_exception(TestConstants.API_EXCEPTION_C)
        self.assertEqual(actual_c, None)

        actual_d = syncerUtils.SyncerUtils.extract_resource_version_from_exception(TestConstants.API_EXCEPTION_D)
        self.assertEqual(actual_d, 53476547)

    def test_is_copied_object(self):
        actual_a = syncerUtils.SyncerUtils.is_copied_object(TestConstants.EVENT_COPIED)
        self.assertTrue(actual_a)

        actual_b = syncerUtils.SyncerUtils.is_copied_object(TestConstants.EVENT_NOT_COPIED)
        self.assertFalse(actual_b)

    def test_purge_dictionary(self):
        actual_annotation_a = syncerUtils.SyncerUtils.purge_dictionary(TestConstants.ANNOTATIONS_E, TestConstants.ANNOTATIONS_E,
                                                                       'kube-syncer/purge-annotations')
        self.assertEqual(actual_annotation_a, TestConstants.EXPECTED_PURGE_LIST)

        actual_annotation_b = syncerUtils.SyncerUtils.purge_dictionary(TestConstants.ANNOTATIONS_B, TestConstants.ANNOTATIONS_B,
                                                                       'kube-syncer/purge-annotations')
        self.assertEqual(actual_annotation_b, TestConstants.ANNOTATIONS_B)

        actual_label_a = syncerUtils.SyncerUtils.purge_dictionary(TestConstants.ANNOTATIONS_E, TestConstants.LABEL_B, 'kube-syncer/purge-labels')
        self.assertEqual(actual_label_a, TestConstants.EXPECTED_PURGE_LABEL_LIST)

        actual_label_b = syncerUtils.SyncerUtils.purge_dictionary(TestConstants.ANNOTATIONS_B, TestConstants.LABEL_A, 'kube-syncer/purge-labels')
        self.assertEqual(actual_label_b, TestConstants.LABEL_A)

    def test_update_healthy(self):
        mock_open = MockOpen()
        with unittest.mock.patch("builtins.open", mock_open):
            syncerUtils.SyncerUtils.update_healthy(MagicMock())

        mock_open.assert_called_once_with(f"{os.getcwd()}/tmp/health", "w")


if __name__ == "__main__":
    unittest.main()
