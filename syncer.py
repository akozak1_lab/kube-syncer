"""Tool to synchronize configmaps and secrets across namespaces.

The class looks at secrets and namespaces and looks for "kube-syncer/auto: ''"
or "kube-syncer/label: 'environment=testing'". The auto option will copy the
object to every namespace in the cluster. If you set the label option it will
only copy to namespaces with the value you set on the annotation.

  Typical usage example:

    mySyncer = Syncer(logger, kubernetes_api, timeout)
    mySyncer.start()
    mySyncer.join_threads()
"""

import logging
import logging.config
import os
import signal
import sys
import threading
import time
from copy import deepcopy
from logging import Formatter

from kubernetes import client, config, watch
from kubernetes.client import ApiException

from syncerUtils import SyncerUtils


class Syncer(object):
    """Manages threads to copy secrets and configmaps.

    Args:
        object: python object class
    """

    def __init__(self, logger, kubernetes_api, timeout_interval):
        """Initialize the Syncer class.

        Args:
            logger: logger to be used in the class
            kubernetes_api: kubernetes api object to be used by the class
            timeout_interval: timeout for watch events in the kubernetes api
        """
        self.should_stop = threading.Event()
        self.should_stop_secret = threading.Event()
        self.should_stop_config_map = threading.Event()
        self.logger = logger
        self.kubernetes_api = kubernetes_api
        self.namespaces = SyncerUtils.filter_active_only_namespaces(self.logger, self.kubernetes_api.list_namespace(watch=False))
        self.last_namespace_resource_version = SyncerUtils.get_most_recent_resource_version_from(self.namespaces.items)
        self.namespace_lock = threading.RLock()
        self.timeout_interval = timeout_interval

    def start(self):
        """Start the processing of events in this class.
        """
        self.start_namespace_thread()
        self.start_resource_sync_threads()

    def stop(self, *argv, **kwargs):
        """Stop the processing of events in this class.
        """
        self.logger.info('Stopping....')
        self.should_stop.set()

    def start_namespace_thread(self):
        """Start a thread to monitor namespace events.
        """
        self.namespace_thread = threading.Thread(target=self.monitor_namespaces, name="Namespace Monitor")
        self.logger.info("Starting Namespace Monitoring")
        self.namespace_thread.start()

    def start_resource_sync_threads(self):
        """Start the threads to monitor secrets and configmaps.
        """
        self.secret_thread = threading.Thread(target=self.monitor_secrets, name="Secrets Monitor")
        self.config_map_thread = threading.Thread(target=self.monitor_config_maps, name="ConfigMap Monitor")

        self.logger.info("Starting Secret Monitoring")
        self.secret_thread.start()
        self.logger.info("Starting ConfigMap Monitoring")
        self.config_map_thread.start()

    def update_health_file(self):
        """Update the health file for the program.
        """
        if not (self.namespace_thread.is_alive() and self.secret_thread.is_alive() and self.config_map_thread.is_alive()):
            self.logger.warning("Threads are not running properly")
            self.logger.warning(f"Namespace Thread is_alive: {self.namespace_thread.is_alive()}")
            self.logger.warning(f"Secret Thread is_alive: {self.secret_thread.is_alive()}")
            self.logger.warning(f"Configmap Thread is_alive: {self.config_map_thread.is_alive()}")
            return

        SyncerUtils.update_healthy(self.logger)

    def join_threads(self):
        """Wait for all threads to join.
        """
        self.secret_thread.join()
        self.config_map_thread.join()
        self.namespace_thread.join()

    def restart_threads(self):
        """Restart the secret and configmap threads.
        """
        if self.should_stop.is_set():
            return

        self.logger.info("Restarting resource sync threads.")
        self.should_stop_secret.set()
        self.should_stop_config_map.set()
        self.secret_thread.join()
        self.config_map_thread.join()

        self.should_stop_secret.clear()
        self.should_stop_config_map.clear()

        self.start_resource_sync_threads()
        self.logger.info("Restart Successful for resource sync threads")

    def get_namespaces(self):
        """Return a list of namespaces.

        Returns:
            namespaces: Returns a list of kubernetes namespaces
        """
        with self.namespace_lock:
            return self.namespaces

    def copy_to_all_namespaces(self, event, method):
        """Copy an event to all namespaces.

        Args:
            event: A kubernetes event to copy
            method: Method name either, set_secret_in_namespace or
            set_config_map_in_namespace
        """
        for namespace in self.get_namespaces().items:
            if not (event['object'].metadata.namespace == namespace.metadata.name):
                method(namespace.metadata.name, event['object'].metadata.name, deepcopy(event['object']))

    def copy_to_labeled_namespaces(self, event, method):
        """Copy an event to any namespace with the matching label.

        Args:
            event: A kubernetes event to copy
            method:  Method name either, set_secret_in_namespace or
            set_config_map_in_namespace
        """
        for namespace in self.get_namespaces().items:
            if SyncerUtils.is_label_in_namespace(namespace, event['object'].metadata.annotations['kube-syncer/label']):
                method(namespace.metadata.name, event['object'].metadata.name, deepcopy(event['object']))

    def set_secret_in_namespace(self, namespace, name, secret):
        """Either replaces or creates a secret in a namespace.

        Args:
            namespace: the kubernetes namespace name
            name: name of the secret to copy
            secret: A kubernetes secret
        """
        secret = SyncerUtils.update_metadata(namespace, secret)
        secrets = self.kubernetes_api.list_namespaced_secret(namespace)

        for s in secrets.items:
            if s.metadata.name != name:
                continue

            if not SyncerUtils.is_equal(s, secret):
                self.logger.debug("Replacing Secret \"{}\" in namespace \"{}\"".format(secret.metadata.name, secret.metadata.namespace))
                self.kubernetes_api.replace_namespaced_secret(name, namespace, secret)
            return

        # If the name above was not found create a new config map
        self.logger.debug("Creating Secret \"{}\" in namespace \"{}\"".format(secret.metadata.name, secret.metadata.namespace))
        self.kubernetes_api.create_namespaced_secret(namespace, secret)

    def set_config_map_in_namespace(self, namespace, name, config_map):
        """Either replaces or creates a configmap in a namespace.

        Args:
            namespace: the kubernetes namespace name
            name: name of the secret to copy
            config_map: A kubernetes configmap
        """
        config_map = SyncerUtils.update_metadata(namespace, config_map)
        config_maps = self.kubernetes_api.list_namespaced_config_map(namespace)

        for s in config_maps.items:
            if s.metadata.name != name:
                continue
            if not SyncerUtils.is_equal(s, config_map):
                self.logger.debug("Replacing ConfigMap \"{}\" in namespace \"{}\"".format(config_map.metadata.name, config_map.metadata.namespace))
                self.kubernetes_api.replace_namespaced_config_map(name, namespace, config_map)
            return

        # If the name above was not found create a new config map
        self.logger.debug("Creating ConfigMap \"{}\" in namespace \"{}\"".format(config_map.metadata.name, config_map.metadata.namespace))
        self.kubernetes_api.create_namespaced_config_map(namespace, config_map)

    def monitor_secrets(self):
        """Watches all secrets for changes and triggers copies.
        """
        w = watch.Watch()
        last_seen_version = None
        while not self.should_stop_secret.is_set() and not self.should_stop.is_set():
            try:
                for event in w.stream(self.kubernetes_api.list_secret_for_all_namespaces, resource_version=last_seen_version,
                                      timeout_seconds=self.timeout_interval):

                    self.logger.debug("Event: {} {} in namespace {} {} version".format(event['type'], event['object'].metadata.name,
                                      event['object'].metadata.namespace, event['object'].metadata.resource_version))

                    if SyncerUtils.is_most_recent_resource_version(event, last_seen_version):
                        last_seen_version = int(event['object'].metadata.resource_version)

                    if event['type'] == 'DELETED' or event['type'] == 'MODIFIED':
                        if SyncerUtils.is_copied_object(event) and SyncerUtils.find_namespace(event['object'], self.namespaces):
                            last_seen_version = None
                            w.stop()
                            break

                    if not event['type'] == 'DELETED':
                        if event['object'].metadata.annotations:
                            if 'kube-syncer/auto' in event['object'].metadata.annotations:
                                self.logger.info("Found \"kube-syncer/auto\" Secret \"{}\" in namespace \"{}\"".format(event['object'].metadata.name,
                                                                                                                       event['object'].metadata.namespace))
                                self.copy_to_all_namespaces(event, self.set_secret_in_namespace)
                            if 'kube-syncer/label' in event['object'].metadata.annotations:
                                self.logger.info("Found \"kube-syncer/label: '{}'\" Secret \"{}\" in namespace \"{}\"".format(
                                    event['object'].metadata.annotations['kube-syncer/label'], event['object'].metadata.name,
                                    event['object'].metadata.namespace))
                                self.copy_to_labeled_namespaces(event, self.set_secret_in_namespace)

                self.logger.debug("Finished watch stream....")
            except ApiException as e:
                if e.status == 410:
                    last_seen_version = SyncerUtils.extract_resource_version_from_exception(e)
                self.logger.error("Caught Exception: {}".format(e.reason))

    def monitor_config_maps(self):
        """Watches all ConfigMaps for changes and triggers copies.
        """
        w = watch.Watch()
        last_seen_version = None
        while not self.should_stop_config_map.is_set() and not self.should_stop.is_set():
            try:
                for event in w.stream(self.kubernetes_api.list_config_map_for_all_namespaces, resource_version=last_seen_version,
                                      timeout_seconds=self.timeout_interval):

                    self.logger.debug("Event: {} {} in namespace {} {} version".format(event['type'], event['object'].metadata.name,
                                      event['object'].metadata.namespace, event['object'].metadata.resource_version))

                    if SyncerUtils.is_most_recent_resource_version(event, last_seen_version):
                        last_seen_version = int(event['object'].metadata.resource_version)

                    if event['type'] == 'DELETED' or event['type'] == 'MODIFIED':
                        if SyncerUtils.is_copied_object(event) and SyncerUtils.find_namespace(event['object'], self.namespaces):
                            last_seen_version = None
                            w.stop()
                            break

                    if not event['type'] == 'DELETED':
                        if event['object'].metadata.annotations:
                            if 'kube-syncer/auto' in event['object'].metadata.annotations:
                                self.logger.info("Found \"kube-syncer/auto\" Secret \"{}\" in namespace \"{}\"".format(event['object'].metadata.name,
                                                                                                                       event['object'].metadata.namespace))
                                self.copy_to_all_namespaces(event, self.set_config_map_in_namespace)

                            if 'kube-syncer/label' in event['object'].metadata.annotations:
                                self.logger.info("Found \"kube-syncer/label: '{}'\" Secret \"{}\" in namespace \"{}\"".format(
                                    event['object'].metadata.annotations['kube-syncer/label'], event['object'].metadata.name,
                                    event['object'].metadata.namespace))
                                self.copy_to_labeled_namespaces(event, self.set_config_map_in_namespace)

                self.logger.debug("Finished watch stream....")
            except ApiException as e:
                if e.status == 410:
                    last_seen_version = SyncerUtils.extract_resource_version_from_exception(e)
                self.logger.error("Caught Exception: {}".format(e.reason))

    def monitor_namespaces(self):
        """Watches all namespaces in a cluster for any events.
        """
        w = watch.Watch()
        while not self.should_stop.is_set():
            try:
                for event in w.stream(self.kubernetes_api.list_namespace, resource_version=self.last_namespace_resource_version,
                                      timeout_seconds=self.timeout_interval):

                    self.logger.debug("Event: {} {} {} version".format(event['type'], event['object'].metadata.name, event['object'].metadata.resource_version))

                    if SyncerUtils.is_most_recent_resource_version(event, self.last_namespace_resource_version):
                        self.last_namespace_resource_version = int(event['object'].metadata.resource_version)

                    if SyncerUtils.is_namespace_event_relevant(self.logger, event, self.namespaces):
                        with self.namespace_lock:
                            self.namespaces = SyncerUtils.filter_active_only_namespaces(self.logger, self.kubernetes_api.list_namespace(watch=False))

                        self.restart_threads()

                self.logger.debug("Finished watch stream....")
            except ApiException as e:
                if e.status == 410:
                    self.last_namespace_resource_version = SyncerUtils.extract_resource_version_from_exception(e)
                self.logger.error("Caught Exception: {}".format(e.reason))


def _fix_kubernetes_logger():  # pragma: no cover
    k8s_log_level = "INFO"

    logging_config = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'simple': {
                'format': '%(asctime)s - %(threadName)-17s - %(levelname)-8s - %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'simple',
            },
        },
        'loggers': {
            'root': {
                'handlers': ['console'],
                'level': os.environ.get('LOGLEVEL', 'DEBUG').upper(),
                'propagate': False,
            },
            'kubernetes': {
                'handlers': ['console'],
                'level': k8s_log_level,
                'propagate': False,
            },
        },
    }

    logging.config.dictConfig(logging_config)


def _setup_logger():  # pragma: no cover
    logger = logging.getLogger()
    log_level = os.environ.get('LOGLEVEL', 'INFO').upper()

    formatter = Formatter('%(asctime)s - %(threadName)-17s - %(levelname)-8s - %(message)s')
    console_handler = logging.StreamHandler(stream=sys.stdout)
    console_handler.setFormatter(formatter)

    logger.setLevel(log_level)
    logger.addHandler(console_handler)

    return logger


def main():  # pragma: no cover
    """Starter method.
    """
    logger = _setup_logger()

    _fix_kubernetes_logger()

    if "LOCALCONFIG" in os.environ:
        logger.info("Loading " + os.path.join(os.environ["LOCALCONFIG"]))
        config.load_kube_config(os.path.join(os.environ["LOCALCONFIG"]))
    else:
        logger.info("Loading service account")
        config.load_incluster_config()

    v1 = client.CoreV1Api()

    timeout = int(os.environ.get('TIMEOUT_INTERVAL', '10'))
    mySyncer = Syncer(logger, v1, timeout)

    mySyncer.start()

    signal.signal(signal.SIGINT, mySyncer.stop)
    signal.signal(signal.SIGTERM, mySyncer.stop)

    while not mySyncer.should_stop.is_set():
        logger.debug("sleeping for {}s".format(timeout))
        mySyncer.update_health_file()
        time.sleep(timeout)

    mySyncer.join_threads()

    logger.info('Exiting....')


if __name__ == '__main__':  # pragma: no cover
    main()
