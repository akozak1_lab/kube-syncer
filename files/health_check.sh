#!/bin/sh

# This script checks if the /app/tmp/health file has been
# updated in the last 10 seconds. 
# 
# Exit Codes:
#     0 = Updating
#     1 = Health file does not exist
#     2 = File is older than 10 seconds

if [ ! -f "/app/tmp/health" ]; then
    exit 1
fi

last_updated=$(($(date +%s) - $(stat /app/tmp/health -c %Y)))

if [ $last_updated -gt 10 ]; then
    exit 2
else
    exit 0
fi
