FROM python:3.9-alpine3.20

LABEL maintainer="Andrew Kozak <andrewkozak11@gmail.com>"
LABEL description="A tool to copy secrets and configmaps across namespaces in Kubernetes."
LABEL url="https://gitlab.com/akozak1_lab/kube-syncer/"

RUN mkdir /app

WORKDIR /app

COPY requirements.txt /app

RUN pip3 install --no-cache-dir -r /app/requirements.txt

COPY files/health_check.sh /app/
COPY syncer.py /app/
COPY syncerUtils.py /app/

CMD ["python", "/app/syncer.py"]
