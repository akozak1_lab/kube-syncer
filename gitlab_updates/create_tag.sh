#!/usr/bin/env bash

curl --request POST --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
"https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/tags?tag_name=${TAG}&ref=main"
